<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class volumeModel extends Model
{
    protected $table = 'm_volume';
    protected $fillable = ['id', 'nama','des'];

    public $timestamps = false;

    public function penulis()
    {
        return $this->hasMany('App\Models\penulisModel', 'vol_id');
    }
}

