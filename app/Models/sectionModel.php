<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sectionModel extends Model
{
    protected $table = 'm_section';
    protected $fillable = ['id', 'nama','des','vol_id'];
    public $timestamps = false;

    public function volume()
    {
      return $this->belongsTo('App\Models\volumeModel', 'vol_id');
    }
}
