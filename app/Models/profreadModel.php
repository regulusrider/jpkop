<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profreadModel extends Model
{
    protected $table = 'm_profread';
    protected $fillable = ['id', 'nama','des','vol_id'];
    public $timestamps = false;

    public function volume()
    {
      return $this->belongsTo('App\Models\volumeModel', 'vol_id');
    }
}
