<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class penulisModel extends Model
{
    protected $table = 'm_penulis';
    protected $fillable = ['id', 'nama','des','vol_id'];
    public $timestamps = false;

    public function volume()
    {
      return $this->belongsTo('App\Models\volumeModel', 'vol_id');
    }
}
