<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fnaskahModel extends Model
{
    protected $table = 'm_full_naskah';
    protected $fillable = ['id', 'vol_id','tgl_masuk','penulis_id','nas_id','hp','p_filter','h_filter','des','se_id','mitbes_a','mitbes_b','tgl_revisi',
    'copy_id','lay_id','pro_id'];
    public $timestamps = false;


    public function volume()
    {
      return $this->belongsTo('App\Models\volumeModel', 'vol_id');
    }

    public function naskah()
    {
      return $this->belongsTo('App\Models\naskahModel', 'nas_id');
    }
    public function penulis()
    {
      return $this->belongsTo('App\Models\penulisModel', 'penulis_id');
    }
    public function mitbesa()
    {
      return $this->belongsTo('App\Models\mitbesModel', 'mitbes_a');
    }
    public function mitbesb()
    {
      return $this->belongsTo('App\Models\mitbesModel', 'mitbes_b');
    }
    public function section()
    {
      return $this->belongsTo('App\Models\sectionModel', 'se_id');
    }
    public function copyid()
    {
      return $this->belongsTo('App\Models\copyidModel', 'copy_id');
    }
    public function layid()
    {
      return $this->belongsTo('App\Models\layidModel', 'lay_id');
    }
    public function profread()
    {
      return $this->belongsTo('App\Models\profreadModel', 'pro_id');
    }
    
}
