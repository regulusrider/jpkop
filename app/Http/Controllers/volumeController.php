<?php

namespace App\Http\Controllers;

use App\Models\volumeModel;
use Illuminate\Http\Request;
use DataTables;
use Redirect,Response;

class volumeController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
if ($request->ajax()) {
return Datatables::of(volumeModel::all())
->addIndexColumn()
->addColumn('action', function($row){

$action = '<a class="btn btn-info" id="show-volume" data-toggle="modal" data-id='.$row->id.'>Show</a>
<a class="btn btn-success" id="edit-volume" data-toggle="modal" data-id='.$row->id.'>Edit </a>
<meta name="csrf-token" content="{{ csrf_token() }}">
<a id="delete-volume" data-id='.$row->id.' class="btn btn-danger delete-volume">Delete</a>';

return $action;

})
->rawColumns(['action'])
->make(true);
}
$volumes = volumeModel::pluck('nama');

return view('volume', compact('volumes'));
}

public function store(Request $request)
{

$r=$request->validate([
'nama' => 'required',
'des' => 'required',

]);
$uId = $request->id;
volumeModel::updateOrCreate(['id' => $uId],['nama' => $request->nama, 'des' => $request->des],);
if(empty($request->id))
$msg = 'created successfully.';
else
$msg = 'data is updated successfully';
return redirect()->route('volume.index')->with('success',$msg);
}

/**
* Display the specified resource.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function show($id)
{
$where = array('id' => $id);
$volume = volumeModel::where($where)->first();
return Response::json($volume);
return view('volume.show',compact('volume'));
}

/**
* Show the form for editing the specified resource.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function edit($id)
{
$where = array('id' => $id);
$volume = volumeModel::where($where)->first();
return Response::json($volume);
}

/**
* Remove the specified resource from storage.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function destroy($id)
{
$volume = volumeModel::where('id',$id)->delete();
return Response::json($volume);
return redirect()->route('volume.index');
}
}