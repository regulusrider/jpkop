<?php

namespace App\Http\Controllers;

use App\Models\sectionModel;
use App\Models\volumeModel;
use Illuminate\Http\Request;
use DataTables;
use Redirect,Response;

class sectionController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
    if ($request->ajax()) {
    return Datatables::of(sectionModel::with('volume')->get())
    ->addIndexColumn()
    ->addColumn('action', function($row){

    $action = '<a class="btn btn-info" id="show-section" data-toggle="modal" data-id='.$row->id.'>Show</a>
    <a class="btn btn-success" id="edit-section" data-toggle="modal" data-id='.$row->id.'>Edit </a>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <a id="delete-section" data-id='.$row->id.' class="btn btn-danger delete-section">Delete</a>';

    return $action;

    })
    ->rawColumns(['action'])
    ->make(true);
}
$volumes = volumeModel::pluck('nama','id');

return view('section', compact('volumes'));
}

public function store(Request $request)
{

$r=$request->validate([
'nama' => 'required',
'des' => 'required',

]);
$uId = $request->id;
sectionModel::updateOrCreate(['id' => $uId],['nama' => $request->nama, 'des' => $request->des,'vol_id' => $request->vol_id],);
if(empty($request->id))
$msg = 'created successfully.';
else
$msg = 'data is updated successfully';
return redirect()->route('section.index')->with('success',$msg);
}

/**
* Display the specified resource.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function show($id)
{
$where = array('id' => $id);
$section = sectionModel::with('volume')->where($where)->first();
return Response::json($section);
return view('section.show',compact('section'));
}

/**
* Show the form for editing the specified resource.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function edit($id)
{
$where = array('id' => $id);
$section = sectionModel::where($where)->first();
return Response::json($section);
}

/**
* Remove the specified resource from storage.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function destroy($id)
{
$section = sectionModel::where('id',$id)->delete();
return Response::json($section);
return redirect()->route('section.index');
}
}