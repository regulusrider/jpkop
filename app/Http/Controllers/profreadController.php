<?php

namespace App\Http\Controllers;

use App\Models\profreadModel;
use App\Models\volumeModel;
use Illuminate\Http\Request;
use DataTables;
use Redirect,Response;

class profreadController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
    if ($request->ajax()) {
    return Datatables::of(profreadModel::with('volume')->get())
    ->addIndexColumn()
    ->addColumn('action', function($row){

    $action = '<a class="btn btn-info" id="show-profread" data-toggle="modal" data-id='.$row->id.'>Show</a>
    <a class="btn btn-success" id="edit-profread" data-toggle="modal" data-id='.$row->id.'>Edit </a>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <a id="delete-profread" data-id='.$row->id.' class="btn btn-danger delete-profread">Delete</a>';

    return $action;

    })
    ->rawColumns(['action'])
    ->make(true);
}
$volumes = volumeModel::pluck('nama','id');

return view('profread', compact('volumes'));
}

public function store(Request $request)
{

$r=$request->validate([
'nama' => 'required',
'des' => 'required',

]);
$uId = $request->id;
profreadModel::updateOrCreate(['id' => $uId],['nama' => $request->nama, 'des' => $request->des,'vol_id' => $request->vol_id],);
if(empty($request->id))
$msg = 'created successfully.';
else
$msg = 'data is updated successfully';
return redirect()->route('profread.index')->with('success',$msg);
}

/**
* Display the specified resource.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function show($id)
{
$where = array('id' => $id);
$profread = profreadModel::with('volume')->where($where)->first();
return Response::json($profread);
return view('profread.show',compact('profread'));
}

/**
* Show the form for editing the specified resource.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function edit($id)
{
$where = array('id' => $id);
$profread = profreadModel::where($where)->first();
return Response::json($profread);
}

/**
* Remove the specified resource from storage.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function destroy($id)
{
$profread = profreadModel::where('id',$id)->delete();
return Response::json($profread);
return redirect()->route('profread.index');
}
}