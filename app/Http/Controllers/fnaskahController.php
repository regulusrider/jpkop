<?php

namespace App\Http\Controllers;

use App\Models\fnaskahModel;
use App\Models\volumeModel;
use App\Models\penulisModel;
use App\Models\naskahModel;
use App\Models\sectionModel;
use App\Models\mitbesModel;
use App\Models\copyidModel;
use App\Models\layidModel;
use App\Models\profreadModel;
use Illuminate\Http\Request;
use DataTables;
use Redirect,Response;

class fnaskahController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
    if ($request->ajax()) {
    return Datatables::of(fnaskahModel::with('volume','penulis','naskah','mitbesa','mitbesb','section','copyid','layid','profread')->get())
    ->addIndexColumn()
    ->addColumn('action', function($row){

    $action = '<a class="btn btn-info" id="show-fnaskah" data-toggle="modal" data-id='.$row->id.'>Show</a>
    <a class="btn btn-success" id="edit-fnaskah" data-toggle="modal" data-id='.$row->id.'>Edit </a>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <a id="delete-fnaskah" data-id='.$row->id.' class="btn btn-danger delete-fnaskah">Delete</a>';

    return $action;

    })
    ->rawColumns(['action'])
    ->make(true);
}
$volumes = volumeModel::pluck('nama','id');
$penuliss = penulisModel::pluck('nama','id');
$naskahs = naskahModel::pluck('nama','id');
$mitbesas = mitbesModel::pluck('nama','id');
$mitbesbs = mitbesModel::pluck('nama','id');
$sections = sectionModel::pluck('nama','id');
$copyids = copyidModel::pluck('nama','id');
$layids = layidModel::pluck('nama','id');
$profreads = profreadModel::pluck('nama','id');


return view('fnaskah', compact('volumes','penuliss','naskahs','mitbesas','mitbesbs','sections','copyids','layids','profreads'));
}

public function store(Request $request)
{

$r=$request->validate([
'vol_id' => 'required',
'tgl_masuk' => 'required',
'penulis_id' => 'required',
'nas_id' => 'required',
'se_id' => 'required',
'des' => 'required',

]);
$uId = $request->id;
fnaskahModel::updateOrCreate(['id' => $uId],['vol_id' => $request->vol_id,'tgl_masuk' => $request->tgl_masuk,'penulis_id' => $request->penulis_id,
'nas_id' => $request->nas_id,'hp' => $request->hp,'p_filter' => $request->p_filter,'h_filter' => $request->h_filter,'des' => $request->des,
'se_id' => $request->se_id,'mitbes_a' => $request->mitbes_a,'mitbes_b' => $request->mitbes_b,'tgl_revisi' => $request->tgl_revisi,
'copy_id' => $request->copy_id,'lay_id' => $request->lay_id,'pro_id' => $request->pro_id  ],);
if(empty($request->id))
$msg = 'created successfully.';
else
$msg = 'data is updated successfully';
return redirect()->route('fnaskah.index')->with('success',$msg);
}

/**
* Display the specified resource.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function show($id)
{
$where = array('id' => $id);
$fnaskah = fnaskahModel::with('volume','penulis','naskah','mitbesa','mitbesb','section','copyid','layid','profread')->where($where)->first();
return Response::json($fnaskah);
return view('fnaskah.show',compact('fnaskah'));
}

/**
* Show the form for editing the specified resource.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function edit($id)
{
$where = array('id' => $id);
$fnaskah = fnaskahModel::where($where)->first();
return Response::json($fnaskah);
}

/**
* Remove the specified resource from storage.
*
* @param int $id
* @return \Illuminate\Http\Response
*/

public function destroy($id)
{
$fnaskah = fnaskahModel::where('id',$id)->delete();
return Response::json($fnaskah);
return redirect()->route('fnaskah.index');
}
}