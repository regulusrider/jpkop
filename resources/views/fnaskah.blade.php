@extends('layouts.master')
@extends('sidebar.dashboard')

@section('content')

<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              
            </nav>
          </div>
          
        </div>
        <!-- Card stats -->
        <div class="container">
          @if(Session::has('alert'))
          <div class="alert alert-{{Session::get('style')}} alert-dismissable fade in">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>    
              <strong>{{Session::get('alert')}}</strong>{{Session::get('msg')}}
          </div>
          @endif
            <div class="card">
              <div class="card-header"> &nbsp;
      
              <a class="btn btn-success mb-2" id="new-fnaskah" data-toggle="modal">Tambah Full Naskah </a>

            </div>
        
              <div class="card-body">
              <div class="row">
                 <div class="col-md-12">
                  <div class="container-fluid">
                  
                        
                    <div class="table-responsive">
                    <table class="table  table-hover data-table">
      
                      <thead class="thead-dark">
                        <tr>
                          <!-- {{-- <th>Nama SKPD</th> --}} -->
                          <th >Volume</th>
                          <th >Tanggal masuk</th>
                          <th >Penulis</th>
                          <th >Naskah</th>
                          <th >Hp</th>
                          <th >p_filter</th>
                          <th >h_filter</th>
                          <th >Deskripsi</th>
                          <th >Section</th>
                          <th >Mitbes A</th>
                          <th >Mitbes B</th>
                          <th >Tanggal Revisi</th>
                          <th >Copy</th>
                          <th >Layout</th>
                          <th >Proof read</th>
                          <th >Action</th>

                        </tr>
                      </thead>
                      
                      </div>
                  </table>
                  </div>
                  </div>
                  </div>
                  </div>
              </div>
              
          </div>
      </div>
   <!-- Add and Edit customer modal -->
<div class="modal fade" id="crud-modal" aria-hidden="true" >
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <h4 class="modal-title" id="userCrudModal"></h4>
    </div>
    <div class="modal-body">
    <form name="userForm" action="{{ route('fnaskah.store') }}" method="POST">
    <input type="hidden" name="id" id="id" >
    @csrf
    <div class="row">
  
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
        <strong>Volume</strong>
<select name="vol_id" id="vol_id" class="form-control">
            @foreach($volumes as $id => $volume)
                <option value="{{ $id }}">
                    {{ $volume }}
                </option>
            @endforeach
        </select></div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
          <strong>Tanggal Masuk :</strong>
          <br>
          <input type="date" name="tgl_masuk" id="tgl_masuk">
          </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Penulis</strong>
    <select name="penulis_id" id="penulis_id" class="form-control">
                @foreach($penuliss as $id => $penulis)
                    <option value="{{ $id }}">
                        {{ $penulis }}
                    </option>
                @endforeach
            </select></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
              <strong>Naskah</strong>
      <select name="nas_id" id="nas_id" class="form-control">
                  @foreach($naskahs as $id => $naskah)
                      <option value="{{ $id }}">
                          {{ $naskah }}
                      </option>
                  @endforeach
              </select></div>
              </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
          <strong>Hp:</strong>
          <input type="text" name="hp" id="hp" class="form-control" placeholder="hp" onchange="validate()" >
          </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>P_filter :</strong>
            <input type="text" name="p_filter" id="p_filter" class="form-control" placeholder="p_filter" onchange="validate()" >
            </div>
            </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
              <strong>H_filter :</strong>
              <input type="text" name="h_filter" id="h_filter" class="form-control" placeholder="h_filter" onchange="validate()" >
              </div>
              </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Deskripsi :</strong>
                <input type="text" name="des" id="des" class="form-control" placeholder="deskripsi" onchange="validate()" >
                </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Section</strong>
          <select name="se_id" id="se_id" class="form-control">
                @foreach($sections as $id => $section)
                    <option value="{{ $id }}">
                        {{ $section }}
                    </option>
                @endforeach
            </select></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
              <strong>Mitbes A</strong>
            <select name="mitbes_a" id="mitbes_a" class="form-control">
                  @foreach($mitbesas as $id => $mitbesa)
                      <option value="{{ $id }}">
                          {{ $mitbesa }}
                      </option>
                  @endforeach
              </select></div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Mitbes B</strong>
              <select name="mitbes_b" id="mitbes_b" class="form-control">
                    @foreach($mitbesbs as $id => $mitbesb)
                        <option value="{{ $id }}">
                            {{ $mitbesb }}
                        </option>
                    @endforeach
                </select></div>
                </div>
             
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                  <strong>Tanggal Revisi :</strong>
                  <br>
                  <input type="date" name="tgl_revisi" id="tgl_revisi">
                  </div>
                  </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
          <strong>Copy Edit :</strong>
        <select name="copy_id" id="copy_id" class="form-control">
              @foreach($copyids as $id => $copyid)
                  <option value="{{ $id }}">
                      {{ $copyid }}
                  </option>
              @endforeach
          </select></div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Layout Edit :</strong>
          <select name="lay_id" id="lay_id" class="form-control">
                @foreach($layids as $id => $layid)
                    <option value="{{ $id }}">
                        {{ $layid }}
                    </option>
                @endforeach
            </select></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
              <strong>Proof read :</strong>
            <select name="pro_id" id="pro_id" class="form-control">
                  @foreach($profreads as $id => $profread)
                      <option value="{{ $id }}">
                          {{ $profread }}
                      </option>
                  @endforeach
              </select></div>
              </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary" >Save</button>
    <a href="{{ route('fnaskah.index') }}" class="btn btn-danger">Cancel</a>
    </div>
    </div>
    </form>
    </div>
    </div>
    </div>
    </div>
    
    <!-- Show user modal -->
    <div class="modal fade" id="crud-modal-show" aria-hidden="true" >
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <h4 class="modal-title" id="userCrudModal-show"></h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2"></div>
    <div class="col-xs-10 col-sm-10 col-md-10 ">
    
    <table class="table-responsive ">
    <tr height="50px"><td><strong>Volume: </strong></td><td id="svolume"></td></tr>
    <tr height="50px"><td><strong>Tanggal Masuk: </strong></td><td id="stgl"></td></tr>
    <tr height="50px"><td><strong>Penulis: </strong></td><td id="spenulis"></td></tr>
    <tr height="50px"><td><strong>Naskah: </strong></td><td id="snaskah"></td></tr>
    <tr height="50px"><td><strong>HP: </strong></td><td id="shp"></td></tr>
    <tr height="50px"><td><strong>p_filter: </strong></td><td id="spfilter"></td></tr>
    <tr height="50px"><td><strong>h_filter: </strong></td><td id="shfilter"></td></tr>
    <tr height="50px"><td><strong>Deskripsi: </strong></td><td id="sdes"></td></tr>
    <tr height="50px"><td><strong>Section: </strong></td><td id="ssec"></td></tr>
    <tr height="50px"><td><strong>Mitbes A: </strong></td><td id="smitbesa"></td></tr>
    <tr height="50px"><td><strong>Mitbes B: </strong></td><td id="smitbesb"></td></tr>
    <tr height="50px"><td><strong>Tanggal Revisi: </strong></td><td id="stglr"></td></tr>
    <tr height="50px"><td><strong>Copy_id: </strong></td><td id="scopyid"></td></tr>
    <tr height="50px"><td><strong>Lay_id: </strong></td><td id="slayid"></td></tr>
    <tr height="50px"><td><strong>Pro_id: </strong></td><td id="sproid"></td></tr>

    <tr><td></td><td style="text-align: right "><a href="{{ route('fnaskah.index') }}" class="btn btn-danger">OK</a> </td></tr>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    
    </body>
    
    <script type="text/javascript">
    
    $(document).ready(function () {
    
    var table = $('.data-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('fnaskah.index') }}",
    columns: [
    {data: 'volume.nama', name: 'volume.nama'},
    {data: 'tgl_masuk', name: 'tgl_masuk'},
    {data: 'penulis.nama', name: 'penulis.nama'},
    {data: 'naskah.nama', name: 'naskah.nama'},
    {data: 'hp', name: 'hp'},
    {data: 'p_filter', name: 'p_filter'},
    {data: 'h_filter', name: 'h_filter'},
    {data: 'des', name: 'des'},
    {data: 'section.nama', name: 'section.nama'},
    {data: 'mitbesa.nama', name: 'mitbesa.nama'},
    {data: 'mitbesb.nama', name: 'mitbesb.nama'},
    {data: 'tgl_revisi', name: 'tg_revisi'},
    {data: 'copy_id', name: 'copyid.nama'},
    {data: 'lay_id', name: 'layid.nama'},
    {data: 'pro_id', name: 'profread.nama'},
    {data: 'action', name: 'action', orderable: false, searchable: false},
    ]
    });
    
    $('#new-fnaskah').click(function () {
    $('#btn-save').val("create-user");
    $('#user').trigger("reset");
    $('#userCrudModal').html("Form");
    $('#crud-modal').modal('show');
    });
    
    $('body').on('click', '#edit-fnaskah', function () {
    var id = $(this).data('id');
    $.get('fnaskah/'+id+'/edit', function (data) {
    $('#userCrudModal').html("Edit");
    $('#btn-update').val("Update");
    $('#btn-save').prop('disabled',false);
    $('#crud-modal').modal('show');
    $('#id').val(data.id);
    $('#vol_id').val(data.vol_id);
    $('#tgl_masuk').val(data.tgl_masuk);
    $('#penulis_id').val(data.penulis_id);
    $('#nas_id').val(data.nas_id);
    $('#hp').val(data.hp);
    $('#p_filter').val(data.p_filter);
    $('#h_filter').val(data.h_filter);
    $('#des').val(data.des);
    $('#se_id').val(data.se_id);
    $('#mitbes_a').val(data.mitbes_a);
    $('#mitbes_b').val(data.mitbes_b);

    $('#tgl_revisi').val(data.tgl_revisi);
    $('#copy_id').val(data.copy_id);
    $('#lay_id').val(data.lay_id);
    $('#pro_id').val(data.pro_id);

    
    })
    });
    $('body').on('click', '#show-fnaskah', function () {
    var fnaskah_id = $(this).data('id');
    $.get('fnaskah/'+fnaskah_id, function (data) {
    
    $('#svolume').html(data.volume.nama);
    $('#stgl').html(data.tgl_masuk);
    $('#spenulis').html(data.penulis.nama);
    $('#snaskah').html(data.naskah.nama);
    $('#shp').html(data.hp);
    $('#spfilter').html(data.p_filter);
    $('#shfilter').html(data.h_filter);
    $('#sdes').html(data.des);
    $('#ssec').html(data.section.nama);
    $('#smitbesa').html(data.mitbesa.nama);
    $('#smitbesb').html(data.mitbesb.nama);
    $('#stglr').html(data.tgl_revisi);
    $('#scopyid').html(data.copyid.nama);
    $('#slayid').html(data.layid.nama);
    $('#sproid').html(data.profread.nama);

    
    })
    $('#userCrudModal-show').html("Details");
    $('#crud-modal-show').modal('show');
    });
    
    $('body').on('click', '#delete-fnaskah', function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    confirm("Are You sure want to delete !");
    
    $.ajax({
    type: "DELETE",
    url: "http://localhost:8000/fnaskah/"+id,
    data: {
    "id": id,
    "_token": token,
    },
    success: function (data) {
    
    //$('#msg').html('Customer entry deleted successfully');
    //$("#customer_id_" + id).remove();
    table.ajax.reload();
    },
    error: function (data) {
    console.log('Error:', data);
    }
    });
    });
    
    });
    
    </script>
@endsection