@extends('layouts.master')
@extends('sidebar.dashboard')

@section('content')

<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              
            </nav>
          </div>
          
        </div>
        <!-- Card stats -->
        <div class="container">
          @if(Session::has('alert'))
          <div class="alert alert-{{Session::get('style')}} alert-dismissable fade in">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>    
              <strong>{{Session::get('alert')}}</strong>{{Session::get('msg')}}
          </div>
          @endif
            <div class="card">
              <div class="card-header"> &nbsp;
      
              <a class="btn btn-success mb-2" id="new-section" data-toggle="modal">Tambah section </a>

            </div>
        
            
        
         
              <div class="card-body">
              <div class="row">
                 <div class="col-md-12">
                  <div class="container-fluid">
                  
                        
                    <div class="table-responsive">
                    <table class="table  table-hover data-table">
      
                      <thead class="thead-dark">
                        <tr>
                          <!-- {{-- <th>Nama SKPD</th> --}} -->
                          <th width="5%" >Volume</th>
                          <th width="20%" >Nama</th>
                          <th width="40%" >Deskripsi</th>
                          <th width="35%" >Action</th>
                        </tr>
                      </thead>
                      
                      </div>
                  </table>
                  </div>
                  </div>
                  </div>
                  </div>
              </div>
              
          </div>
      </div>
   <!-- Add and Edit customer modal -->
<div class="modal fade" id="crud-modal" aria-hidden="true" >
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <h4 class="modal-title" id="userCrudModal"></h4>
    </div>
    <div class="modal-body">
    <form name="userForm" action="{{ route('section.store') }}" method="POST">
    <input type="hidden" name="id" id="id" >
    @csrf
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
    <strong>Nama:</strong>
    <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" onchange="validate()" >
    </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
    <strong>Deskripsi</strong>
    <input type="text" name="des" id="des" class="form-control" placeholder="dekkripsi" onchange="validate()">
    </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
        <strong>volume</strong>
<select name="vol_id" id="vol_id" class="form-control">
            @foreach($volumes as $id => $volume)
                <option value="{{ $id }}">
                    {{ $volume }}
                </option>
            @endforeach
        </select>        </div>
        </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary" >Save</button>
    <a href="{{ route('section.index') }}" class="btn btn-danger">Cancel</a>
    </div>
    </div>
    </form>
    </div>
    </div>
    </div>
    </div>
    
    <!-- Show user modal -->
    <div class="modal fade" id="crud-modal-show" aria-hidden="true" >
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <h4 class="modal-title" id="userCrudModal-show"></h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2"></div>
    <div class="col-xs-10 col-sm-10 col-md-10 ">
    
    <table class="table-responsive ">
    <tr height="50px"><td><strong>Name:</strong></td><td id="snama"></td></tr>
    <tr height="50px"><td><strong>Deskripsi:</strong></td><td id="sdes"></td></tr>
    <tr height="50px"><td><strong>Volume</strong></td><td id="svolume"></td></tr>
    
    <tr><td></td><td style="text-align: right "><a href="{{ route('section.index') }}" class="btn btn-danger">OK</a> </td></tr>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    
    </body>
    
    <script type="text/javascript">
    
    $(document).ready(function () {
    
    var table = $('.data-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('section.index') }}",
    columns: [
    {data: 'volume.nama', name: 'volume.nama'},
    {data: 'nama', name: 'nama'},
    {data: 'des', name: 'des'},
    {data: 'action', name: 'action', orderable: false, searchable: false},
    ]
    });
    
    $('#new-section').click(function () {
    $('#btn-save').val("create-user");
    $('#user').trigger("reset");
    $('#userCrudModal').html("Form");
    $('#crud-modal').modal('show');
    });
    
    $('body').on('click', '#edit-section', function () {
    var id = $(this).data('id');
    $.get('section/'+id+'/edit', function (data) {
    $('#userCrudModal').html("Edit");
    $('#btn-update').val("Update");
    $('#btn-save').prop('disabled',false);
    $('#crud-modal').modal('show');
    $('#id').val(data.id);
    $('#nama').val(data.nama);
    $('#des').val(data.des);
    $('#vol_id').val(data.vol_id);

    
    })
    });
    $('body').on('click', '#show-section', function () {
    var section_id = $(this).data('id');
    $.get('section/'+section_id, function (data) {
    
    $('#snama').html(data.nama);
    $('#sdes').html(data.des);
    $('#svolume').html(data.volume.nama);

    
    })
    $('#userCrudModal-show').html("Details");
    $('#crud-modal-show').modal('show');
    });
    
    $('body').on('click', '#delete-section', function () {
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    confirm("Are You sure want to delete !");
    
    $.ajax({
    type: "DELETE",
    url: "http://localhost:8000/section/"+id,
    data: {
    "id": id,
    "_token": token,
    },
    success: function (data) {
    
    //$('#msg').html('Customer entry deleted successfully');
    //$("#customer_id_" + id).remove();
    table.ajax.reload();
    },
    error: function (data) {
    console.log('Error:', data);
    }
    });
    });
    
    });
    
    </script>
@endsection