<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailProsesNaskahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('m_detail_proses_naskah')){

        Schema::create('m_detail_proses_naskah', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('des');
            $table->timestamps();



        });
    }
        
        Schema::table('m_detail_proses_naskah', function (Blueprint $table){
            $table->foreignId('vol_id')->constrained('m_volume')->onDelete('cascade');
            $table->foreignId('nas_id')->constrained('m_naskah')->onDelete('cascade');
            $table->foreignId('pros_id')->constrained('m_proses')->onDelete('cascade');

        
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_proses_naskah');
    }
}
