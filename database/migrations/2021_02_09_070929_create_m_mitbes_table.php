<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMMitbesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('m_mitbes')){

        Schema::create('m_mitbes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('des');
            $table->timestamps();

        });
    }
        Schema::table('m_mitbes', function (Blueprint $table){
            $table->foreignId('vol_id')->constrained('m_volume')->onDelete('cascade');
        });
        
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_mitbes');
    }
}
