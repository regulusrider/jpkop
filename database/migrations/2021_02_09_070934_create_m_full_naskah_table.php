<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMFullNaskahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('m_full_naskah')){
        Schema::create('m_full_naskah', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('tgl_masuk');
            $table->string('hp');
            $table->string('p_filter');
            $table->string('h_filter');
            $table->string('des');
            $table->datetime('tgl_revisi');
            $table->timestamps();

        });
    }
        Schema::table('m_full_naskah', function (Blueprint $table){
            $table->foreignId('vol_id')->constrained('m_volume')->onDelete('cascade');
            $table->foreignId('penulis_id')->constrained('m_penulis')->onDelete('cascade');
            $table->foreignId('nas_id')->constrained('m_naskah')->onDelete('cascade');
            $table->foreignId('se_id')->constrained('m_section')->onDelete('cascade');
            $table->foreignId('mitbes_a')->constrained('m_mitbes')->onDelete('cascade');
            $table->foreignId('mitbes_b')->constrained('m_mitbes')->onDelete('cascade');
            $table->foreignId('copy_id')->constrained('m_copyedit')->onDelete('cascade');
            $table->foreignId('lay_id')->constrained('m_layoutedit')->onDelete('cascade');
            $table->foreignId('pro_id')->constrained('m_profread')->onDelete('cascade');

        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_full_naskah');
    }
}
