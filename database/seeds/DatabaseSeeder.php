<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_level')->insert([
            'nama' => 'admin',
            'des' => 'des',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_level')->insert([
            'nama' => 'user',
            'des' => 'des',
            'created_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'test@gmail.com',
            'password' => bcrypt('12345678'),
            'level_id'=> '1',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_volume')->insert([
            'nama' => 'volume 1',
            'des' => 'des',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_volume')->insert([
            'nama' => 'volume 2',
            'des' => 'des',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_profread')->insert([
            'nama' => 'profread 1',
            'des' => 'des',
            'vol_id' => '1',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_profread')->insert([
            'nama' => 'profread 2',
            'des' => 'des',
            'vol_id' => '2',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_layoutedit')->insert([
            'nama' => 'layout 1',
            'des' => 'des',
            'vol_id' => '1',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_layoutedit')->insert([
            'nama' => 'layout 2',
            'des' => 'des',
            'vol_id' => '2',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_copyedit')->insert([
            'nama' => 'copy 1',
            'des' => 'des',
            'vol_id' => '1',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_copyedit')->insert([
            'nama' => 'copy 2',
            'des' => 'des',
            'vol_id' => '2',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_proses')->insert([
            'nama' => 'proses 1',
            'des' => 'des',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_proses')->insert([
            'nama' => 'proses 2',
            'des' => 'des',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_section')->insert([
            'nama' => 'section 1',
            'des' => 'des',
            'vol_id' => '1',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_section')->insert([
            'nama' => 'section 2',
            'des' => 'des',
            'vol_id' => '2',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_mitbes')->insert([
            'nama' => 'mitbes 1',
            'des' => 'des',
            'vol_id' => '1',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_mitbes')->insert([
            'nama' => 'mitbes 2',
            'des' => 'des',
            'vol_id' => '2',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_penulis')->insert([
            'nama' => 'penulis 1',
            'des' => 'des',
            'vol_id' => '1',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_penulis')->insert([
            'nama' => 'penulis 2',
            'des' => 'des',
            'vol_id' => '2',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_naskah')->insert([
            'nama' => 'naskah 1',
            'des' => 'des',
            'vol_id' => '1',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_naskah')->insert([
            'nama' => 'naskah 2',
            'des' => 'des',
            'vol_id' => '2',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_full_naskah')->insert([
            'vol_id' => '1',
            'tgl_masuk' => Carbon::now(),
            'penulis_id' => '1',
            'nas_id' => '1',
            'hp' => '08',
            'p_filter' => 'filter',
            'h_filter' => 'filter',
            'des' => 'des',
            'se_id' => '1',
            'mitbes_a' => '1',
            'mitbes_b' => '1',
            'tgl_revisi' => Carbon::now(),
            'copy_id' => '1',
            'lay_id' => '1',
            'copy_id' => '1',
            'pro_id' => '1',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_full_naskah')->insert([
            'vol_id' => '2',
            'tgl_masuk' => Carbon::now(),
            'penulis_id' => '2',
            'nas_id' => '2',
            'hp' => '08',
            'p_filter' => 'filter',
            'h_filter' => 'filter',
            'des' => 'des',
            'se_id' => '2',
            'mitbes_a' => '2',
            'mitbes_b' => '2',
            'tgl_revisi' => Carbon::now(),
            'copy_id' => '2',
            'lay_id' => '2',
            'copy_id' => '2',
            'pro_id' => '2',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_detail_proses_naskah')->insert([
            'nama' => 'detail 1',
            'des' => 'des',
            'vol_id' => '1',
            'nas_id' => '1',
            'pros_id' => '1',
            'created_at' => Carbon::now(),
        ]);
        DB::table('m_detail_proses_naskah')->insert([
            'nama' => 'detail 2',
            'des' => 'des',
            'vol_id' => '2',
            'nas_id' => '2',
            'pros_id' => '2',
            'created_at' => Carbon::now(),
        ]);
    }
}
