<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class jpkop extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_level')->insert([
            'nama' => 'admin',
            'des' => 'des',
        ]);
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'test@gmail.com',
            'password' => bcrypt('12345678'),
            'level_id'=> '1',
        ]);
    }
}